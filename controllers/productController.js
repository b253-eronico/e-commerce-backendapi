const Product = require("../models/Product");
const User = require("../models/User");
const bcrypt = require("bcrypt");
const Order = require('../models/Order');


// ====================== Create Product ====================
module.exports.createProduct = (reqBody) => {

  const product = new Product({
    name: reqBody.name,

    description: reqBody.description,

    price: reqBody.price

  });

  return product.save().then((savedProduct) => savedProduct);
};




// ========== Retrieve all products =====================
module.exports.getAllProducts = () => {

  return Product.find({}).then(result => result)
    .catch(err => re.send(err));
}

// ========== Retrieve all active products =====================
module.exports.getAllActiveProducts = () => {

  return Product.find({ isActive : true}).then(result => result)
    .catch(err => re.send(err));
}

// ================ Get Product =========================
module.exports.getProduct = (reqParams) => {

  return Product.findById(reqParams.productId).then(result => {
    return result
  }).catch(err => re.send(err))
};


// ================= Update Product =====================
module.exports.updateProduct = (reqParams, reqBody) => {
  let updatedProduct = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  };

  return Product.findByIdAndUpdate(reqParams.productId, updatedProduct, {
    new: true,
  })
    .then((data) => {
      // Return true if the update operation was successful
      return true;
    })
    .catch((err) => {
      return res.send(err);
    });
};

// module.exports.updateProduct = async (req, res) => {
//   const productId = req.params.productId;

//   // Check if authenticated user is an admin
//   if (!req.user.isAdmin) {
//     return res.status(403).json({ message: "Access denied" });
//   }

//   try {
//     const updatedProduct = await Product.findByIdAndUpdate(
//       productId,
//       req.body,
//       { new: true }
//     );
//     res.status(200).json(updatedProduct);
//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ message: "Internal server error" });
//   }
// };


// ================= Archiving Products ====================
module.exports.statusProduct = (reqParams) => {
  return Product.findById(reqParams.productId)
    .then(product => {
      product.isActive = !product.isActive; // toggle the isActive flag
      return product.save();
    })
    .then(updatedProduct => {
      return true;
    })
    .catch(err => {
      return err;
    });
};

// =================== Activate Product ==============
module.exports.statProduct = (reqParams) => {

  let updateActiveField = {
    isActive : true
  };

  return Product.findByIdAndUpdate(reqParams.productId, updateActiveField, {new : true})
    .then(updatedProduct => { //for future me -to display full data, must return updated product data

      return updatedProduct;
    })

    .catch(err => {
      return err;
    });
};

// ================ order =========================

module.exports.test = async (reqBody, res) => {
  try {
    let total = 0;
    let newOrder = new Order({
      userId: reqBody.userId,
      products: [{
        productId: reqBody.productId,
        productName: reqBody.productName,
        price: reqBody.price,
        quantity: reqBody.quantity,
      }],
    });

    // for (let x = 0; x < reqBody.products.length; x++) {
    //   const getProducts = await Product.findById(reqBody.products[x].productId);
    //   newOrder.products.push({
    //     productId: reqBody.products[x].productId,
    //     productName: reqBody.products[x].productName,
    //     price: reqBody.products[x].price,
    //     quantity: reqBody.products[x].quantity,
    //   });
    // }

    // newOrder.userId = reqBody.userId;

    await newOrder.save()

    res.status(201).send(true)
    } catch (error) {
      console.log(error);
      res.status(500).json({
        status: "error",
        msg: "Failed to create order",
        error: error.message,
      });
    }
};




