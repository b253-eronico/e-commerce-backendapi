
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, "User ID is required"],
  },
  products: [
    {
      productId: {
        type: String,
        required: [true, "Product ID is required"],
      },
      productName: {
        type: String,
        required: [true, "Product Name is required"],
      },
      price: {
        type: Number,
        required: [true, "Quantity is required"],
      },
      quantity: {
        type: Number,
        required: [true, "Quantity is required"],
      },
      subtotal: {
        type: Number
      },
    },
  ],
  totalAmount: {
    type: Number
  },
  purchasedOn: {
    type: Date,
    default: new Date(),
  },
});

orderSchema.pre('save', function(next) {

  let totalAmount = 0;

  this.products.forEach((product) => {

    product.subtotal = Math.round(product.price * product.quantity);

    totalAmount += product.subtotal;
  });
  this.totalAmount = totalAmount; 
  next();
});

module.exports = mongoose.model("Order", orderSchema)