const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");
const User = require('../models/User');


// =========== Check Email =======
router.post("/checkEmail", (req,res) => {

    // The full route to access this is "http://localhost:4000/users/checkEmail" where the "/users" was defined in our "index.js" file
    // The "then" method uses the result from the controller function and sends it back to the frontend application via the "res.send" method
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});

// =========== Register ===========
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});




// ========== User Authentication ==========
router.post('/login', (req, res) => {

  userController.loginUser(req.body).then(resultFromController => {

      if (resultFromController.error) { //codes to show errors if output shows blank array

        res.status(401).send(resultFromController);
      } else {

        res.send(resultFromController);
      }
    }).catch(err => {

      console.log(err); //codes to show errors if output shows blank array

      res.send({ error: 'Internal Server Error' });
    });
});



// ================= Retrieve User Details =============
router.get("/profile", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization);

  userController.getUser({ userId: userData.id }).then(resultFromController => res.send(resultFromController))

    .catch(err => res.send(err));

});


// ===================== SET as ADMIN =====================

router.put("/setadmin/:userId", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController.setAsAdmin(req, res);
});




// ==================== CART ==================

router.get('/:userId/cart', async (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    const resultFromController = await userController.getOrder({ userId: userData.id });
    res.send(resultFromController);
  } catch (err) {
    res.send(err);
  }
});


// ============ All Users ================

router.get("/allusers", auth.verify, (req,res) => { //need middleware auth

  const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin){
    userController.getAllUsers().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
  } else {

    return res.send(false)
  }

});


// ======================== All ORDERs ===================

router.get("/orders", auth.verify, (req, res) => { //need middleware auth

  const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin){
    userController.getAllOrders().then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
  } else {

    return res.send(false)
  }

});

module.exports = router;


